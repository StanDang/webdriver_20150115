package JiraTesting;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


public class LoginPage {
	private final WebDriver driver;
	By usernameLocator = By.id("username");
	By passwordLocator = By.id("password");
	By signinButtonLocator = By.id("login-submit");

    public LoginPage(WebDriver driver) {
        this.driver = driver;
        
        if (!"Sign in to continue".equals(driver.getTitle())) {
        	System.out.println("Page title is: " + driver.getTitle());
        	throw new IllegalStateException("This is not Login page");
        }

    }
        
    
    public SummaryPage logIn(WebDriver driver, String userName,String passWord ){
    	driver.findElement(usernameLocator).sendKeys(userName);
    	driver.findElement(passwordLocator).sendKeys(passWord);	     
    	driver.findElement(signinButtonLocator).click();
    	
    	return new SummaryPage(driver);
    }
    

}
