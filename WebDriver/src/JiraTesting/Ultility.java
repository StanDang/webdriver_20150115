package JiraTesting;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Ultility {
	public static boolean WaitForPageDone(WebDriver driver, final By element) {
		WebDriverWait waiter = (WebDriverWait) new WebDriverWait(driver, 30)
				.withMessage("driver.getWindowHandles().size() is "
						+ driver.getWindowHandles().size()
						+ ". CreateIssuePage exists? "
						+ driver.findElements( element).toString());

		return waiter.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver input) {
				if (input.findElements( element).size() == 0)
					return true;
				return false;
			}
		});
	}
	
	public static boolean WaitForPageLoad(WebDriver driver, final By element, final String verifyText) {
		WebDriverWait waiter = (WebDriverWait) new WebDriverWait(driver, 5)
				.withMessage("wait for page to display " + verifyText);

		return waiter.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver input) {
				if (input.findElements(element).get(0).getText().equalsIgnoreCase(verifyText))
					return true;
				return false;				
			}
		});
	}
	
//	public static void waitForWindowsCount(WebDriver driver, final int count) {
//	WebDriverWait waiter = (WebDriverWait) new WebDriverWait(driver, 30)
//			.withMessage("driver.getWindowHandles().size() is "
//					+ driver.getWindowHandles().size()
//					+ ". Expected size is " + count);
//
//	waiter.until(new ExpectedCondition<Boolean>() {
//		@Override
//		public Boolean apply(WebDriver input) {
//			if (input.getWindowHandles().size() == count)
//				return true;
//			return false;
//		}
//	});
//
//}

}
