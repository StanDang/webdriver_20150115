package JiraTesting;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SummaryPage {
	private final WebDriver driver;
	By issueLinkLocator = By.id("com.atlassian.jira.jira-projects-plugin:issues-panel-panel");
	By reportedByMeFilterLinkLocator = By.id("filter_reportedbyme");
	By createIssueButtonLocator = By.id("create_link");
	By keyValLocator = By.id("key-val");
	//By keyValLocator = By.className("issue-created-key issue-link");
	//By keyValLocator = By.xpath("//*[@id='jira']/div[10]/div/a");
	//By keyValLocator = By.xpath("//body[@id='jira']/div[@class='global-msg']/div[@class='aui-message success closeable']/a[@class='issue-created-key issue-link']");
	By summaryValLocator = By.id("summary-val");
	By newPostedDefectLocator = By.xpath("//*[@id='content']/div[1]/div[4]/div/div/div/div/div/div[1]/div[1]/div[2]/ol/li[1]/a/span[1]");
	String defectIDAttribute = "data-issue-key";
	
	By quickSearchInputLocator = By.id("quickSearchInput");
	By quickSearchButtonLocator = By.xpath("//form[@id='quicksearch']/input[2]");
	By userProfileLocator = By.id("user-options");
	By logOutLocator = By.id("log_out");
	By refreshIconLocator = By.xpath("//*[@id='content']/div[1]/div[4]/div/div/div/div/div/div[1]/div[3]/div[1]/a");
	

    public SummaryPage(WebDriver driver) {
        this.driver = driver;
        
        if (!"A Test Project - Atlassian JIRA".equals(driver.getTitle())) {
        	System.out.println("Page title is: " + driver.getTitle());
        	throw new IllegalStateException("This is not SummaryPage page");
        }
    }
    
    public CreateIssuePage openCreateIssuePage(WebDriver driver) {
    	driver.findElement(issueLinkLocator).click();
    	driver.findElement(reportedByMeFilterLinkLocator).click();
	    driver.findElement(createIssueButtonLocator).click();
	    
	    return new CreateIssuePage(driver);
    }

	public String getDefectID(WebDriver driver2) {
		return driver.findElement(keyValLocator).getAttribute(defectIDAttribute);
	}

	public void verifyPostedDefect(WebDriver driver, String defectID,
			String defectSummary) {
		
		if(driver.findElement(summaryValLocator).getText().equalsIgnoreCase(defectSummary))
	     {
	    	 System.out.println("Test Case #01: Issue is succesfully posted with Summary: " + defectSummary + " and ID: " + defectID);
	     }
	     else
	     {
	    	 System.out.println("Error at Test Case #01");
	     } 
		
	}

	public SearchResultPage searchWithDefectID(WebDriver driver, String defectID) {
		driver.findElement(quickSearchInputLocator).sendKeys(defectID);
		driver.findElement(quickSearchInputLocator).submit();
	     		
		return new SearchResultPage(driver);
	}

	public void displayNewDefect(WebDriver driver, String verifyText) {
		// TODO Auto-generated method stub
		driver.findElement(newPostedDefectLocator).click();
		Ultility.WaitForPageLoad(driver, summaryValLocator,verifyText);		
	}

}
