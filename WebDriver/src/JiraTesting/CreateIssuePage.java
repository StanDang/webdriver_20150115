package JiraTesting;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CreateIssuePage {
	private final WebDriver driver;
	By summaryTextLocator = By.id("summary");
	By submitButtonLocator = By.id("create-issue-submit");
	By createAnotherChkBoxLocator = By.id("qf-create-another");
	By createIssueDialog = By.id("create-issue-dialog"); 
	

	public CreateIssuePage(WebDriver driver) {
		this.driver = driver;

		/*
		 * WebElement title = driver.findElement(By.tagName("h2"));
		 * 
		 * if(!title.getText().equals("Create Issue")) {
		 * System.out.println("Page title is: " + driver.getTitle()); throw new
		 * IllegalStateException("This is not CreateIssuePage page"); }
		 */
	}

	public void submitIssue(WebDriver driver, String defectSummary) {

		driver.findElement(summaryTextLocator).sendKeys(defectSummary);
		driver.findElement(submitButtonLocator).click();

	}

	public void waitCreateIssueDone(WebDriver driver) {
		Ultility.WaitForPageDone(driver, createIssueDialog);
	}


}
