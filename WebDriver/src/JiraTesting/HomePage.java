package JiraTesting;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HomePage {
	private final WebDriver driver;
	By userOptionLocator = By.id("user-options");

    public HomePage(WebDriver driver) {
        this.driver = driver;
        if (!"A Test Project - Atlassian JIRA".equals(driver.getTitle())) {
        	System.out.println("Page title is: " + driver.getTitle());
        	throw new IllegalStateException("This is not HomePage page");
        }
    }
       
    
    public LoginPage clickLogin(WebDriver driver){
    	driver.findElement(userOptionLocator).click();

        // Return a new page object representing the destination. Should the login page ever
        // go somewhere else (for example, a legal disclaimer) then changing the method signature
        // for this method will mean that all tests that rely on this behaviour won't compile.
        return new LoginPage(driver);  
    }

}
