package JiraTesting;

import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

/*
 * Author: Phat, Dang Quang
 * Email: dangphat29988@gmail.com 
 * Modified Date: 15Jan2015
 * 1. Prerequisite:
 * - Add libraries for WebDriver
 * 2. Assumption: 
 * - Creation, Search and Edition of the defect base on required field which is 'Summary' and defect ID. 
 * 3. Run 
 * - Run example.java
 * - Before running, change the 'chromeLocation' variable to your chrome location.
 */
public class Example {
	private static WebDriver driver = null;

	public static void main(String[] args) {

		String chromeLocation = "C:\\Users\\User7\\Local Settings\\Application Data\\Google\\Chrome\\Application\\chromedriver.exe";
		String userName = "happycook88@gmail.com";
		String passWord = "Stan@29988";
		String defectSummary = "Summary_00"
				+ RandomStringUtils.randomAlphanumeric(20).toUpperCase();
		;
		String defectSummary_update = defectSummary + " updated";
		String defectID = "";
		String siteLink = "https://jira.atlassian.com/browse/TST";
		Defect defect = new Defect(defectID, defectSummary);

		System.setProperty("webdriver.chrome.driver", chromeLocation);

		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(100, TimeUnit.SECONDS);
		driver.get(siteLink);

		// Use page Object library now

		HomePage pHome = new HomePage(driver);
		LoginPage pLogin = pHome.clickLogin(driver);
		SummaryPage pSummary = pLogin.logIn(driver, userName, passWord);

		// Test Case #1: verify Jira can create new defect
		CreateIssuePage pCreateIssue = pSummary.openCreateIssuePage(driver);
		pCreateIssue.submitIssue(driver, defect.getDefectSummary());
		pCreateIssue.waitCreateIssueDone(driver);
		driver.navigate().refresh();
		pSummary.displayNewDefect(driver, defect.getDefectSummary());
		defect.setDefectID(pSummary.getDefectID(driver));
		pSummary.verifyPostedDefect(driver, defect.getDefectID(),
				defect.getDefectSummary());

		// Test Case #2: Existing issues can be found via JIRA�s search
		SearchResultPage pSearchResult = pSummary.searchWithDefectID(driver,
				defect.getDefectID());
		pSearchResult.verifySearchDefectID(driver, defect.getDefectID(),
				defect.getDefectSummary());

		// Test Case #3: Existing issues can be updated
		defect.setDefectSummary(defectSummary_update);
		EditIssuePage pEditIssue = pSearchResult.clickEdit(driver);
		pEditIssue.editDefect(driver, defect.getDefectSummary());
		pEditIssue.waitEditDefectDone(driver);
		pSearchResult.verifyEditDefect(driver, defect.getDefectID(),
				defect.getDefectSummary());

		driver.quit();

	}

}
