package JiraTesting;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;


public class EditIssuePage {

	private final WebDriver driver;
	By titleLocator = By.tagName("h2");
	By summaryTextLocator = By.id("summary");
	By submitEditLocator = By.id("edit-issue-submit");
	By editIssueDialog = By.id("edit-issue-dialog");
	
	

    public EditIssuePage(WebDriver driver) {
        this.driver = driver;
        //
    }


	public void editDefect(WebDriver driver2, String defectSummary_update) {
		driver.findElement(summaryTextLocator).sendKeys(defectSummary_update);
		driver.findElement(submitEditLocator).click();		 
		
	}

	public void waitEditDefectDone(WebDriver driver) {
		Ultility.WaitForPageDone(driver, editIssueDialog);
//		WebDriverWait waiter = (WebDriverWait) new WebDriverWait(driver, 30)
//				.withMessage("driver.getWindowHandles().size() is "
//						+ driver.getWindowHandles().size()
//						+ ". CreateIssuePage exists? "
//						+ driver.findElements(By.id("lblOrderHeaderSaving")).toString()
//						);
//
//		waiter.until(new ExpectedCondition<Boolean>() {
//			@Override
//			public Boolean apply(WebDriver input) {
//				if (input.findElements(By.id("create-issue-dialog")).size() == 0)
//					return true;
//				return false;
//			}
//		});
	}
    
}
