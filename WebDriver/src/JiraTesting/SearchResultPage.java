package JiraTesting;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SearchResultPage {
	private final WebDriver driver;
	By keyValLocator = By.id("key-val");
	By editIssueButtonLocator = By.id("edit-issue");
	By summaryValLocator = By.id("summary-val");

    public SearchResultPage(WebDriver driver) {
        this.driver = driver;
        /*if (!"A Test Project - Atlassian JIRA".equals(driver.getTitle())) {
        	System.out.println("Page title is: " + driver.getTitle());
        	throw new IllegalStateException("This is not HomePage page");
        }*/
    }

	public void verifySearchDefectID(WebDriver driver, String defectID,
			String defectSummary) {
		if(driver.findElement(keyValLocator).getText().equalsIgnoreCase(defectID)
	    		 && driver.findElement(summaryValLocator).getText().equalsIgnoreCase(defectSummary)
	    		 )
	     {
	    	 System.out.println("Test Case #02: Issue is succesfully found with Summary: " + defectSummary + " and ID: " + defectID);
	     }
	     else
	     {
	    	 System.out.println("Error at Test Case #02");
	     }
		
	}

	public EditIssuePage clickEdit(WebDriver driver2) {

		driver.findElement(editIssueButtonLocator).click();	
		
		return new EditIssuePage(driver);
	}

	public void verifyEditDefect(WebDriver driver, String defectID,
			String defectSummary_update) {
		if (
	    		driver.findElement(summaryValLocator).getText().equals(defectSummary_update)
					&& driver.findElement(keyValLocator).getText().equals(defectID)
	    		 )
	     {
	    	 System.out.println("Test Case #03: Issue is succesfully updated with Summary: " + defectSummary_update + " and ID: " + defectID);
	     }
	     else
	     {
	    	 System.out.println("Error at Test Case #03 with Summary: " + driver.findElement(summaryValLocator).getText() + " and ID: " + driver.findElement(keyValLocator).getText());
	     }
		
	}
    
    
}
