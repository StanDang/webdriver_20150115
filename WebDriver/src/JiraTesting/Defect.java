package JiraTesting;

public class Defect {
	private String defectID;
	private String defectSummary;
	
	public Defect(String _ID, String _Summary){
		defectID = _ID;
		defectSummary = _Summary;
	}	
	
	public void setDefectID(String _ID){
		defectID = _ID;
	}
	
	public void setDefectSummary(String _Summary){
		defectSummary = _Summary;
	}
	
	public String getDefectID(){
		return defectID;
	}
	
	public String getDefectSummary(){
		return defectSummary;
	}
}